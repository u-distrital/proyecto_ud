<?php

    include("../../bd.php");

    if($_POST){

        $conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        // Recolecta los datos del método POST
        $correo = isset($_POST["correo"]) ? $_POST["correo"] : "";
        $contrasena = isset($_POST["contrasena"]) ? $_POST["contrasena"] : "";
        $celular = isset($_POST["celular"]) ? $_POST["celular"] : "";
        $fechaDeRegistro = isset($_POST["fechaDeRegistro"]) ? $_POST["fechaDeRegistro"] : "";
        $rol_idRol = isset($_POST["rol_idRol"]) ? $_POST["rol_idRol"] : "";
        $persona_numeroDocumento = isset($_POST["persona_numeroDocumento"]) ? $_POST["persona_numeroDocumento"] : "";

        try {
            // Llamar al procedimiento almacenado "personaInsertar"
            $sentenciaPersona = $conexion->prepare("CALL personaInsertar(:numeroDocumento, :primerNombre, :segundoNombre, :primerApellido, :segundoApellido, :fechaDeNacimiento, :direccion, :telefono, :estadoCivil_idEstadoCivil, :tipoDeSangre_idTipoDeSangre, :genero_idGenero, :paisDeNacimiento_idPais, :tipoDeDocumento_idTipoDeDocumento, :estrato_idEstrato)");
            $sentenciaPersona->bindParam(":numeroDocumento", $_POST["numeroDocumento"]);
            $sentenciaPersona->bindParam(":primerNombre", $_POST["primerNombre"]);
            $sentenciaPersona->bindParam(":segundoNombre", $_POST["segundoNombre"]);
            $sentenciaPersona->bindParam(":primerApellido", $_POST["primerApellido"]);
            $sentenciaPersona->bindParam(":segundoApellido", $_POST["segundoApellido"]);
            $sentenciaPersona->bindParam(":fechaDeNacimiento", $_POST["fechaNacimiento"]);
            $sentenciaPersona->bindParam(":direccion", $_POST["direccion"]);
            $sentenciaPersona->bindParam(":telefono", $_POST["telefono"]);
            $sentenciaPersona->bindParam(":estadoCivil_idEstadoCivil", $_POST["estadoCivil"]);
            $sentenciaPersona->bindParam(":tipoDeSangre_idTipoDeSangre", $_POST["rh"]);
            $sentenciaPersona->bindParam(":genero_idGenero", $_POST["genero"]);
            $sentenciaPersona->bindParam(":paisDeNacimiento_idPais", $_POST["paisNacimiento"]);
            $sentenciaPersona->bindParam(":tipoDeDocumento_idTipoDeDocumento", $_POST["tipoDocumento"]);
            $sentenciaPersona->bindParam(":estrato_idEstrato", $_POST["estrato"]);
            $sentenciaPersona->execute();
    
            // Obtener el número de documento insertado en la tabla "persona"
            $numeroDocumento = $_POST["numeroDocumento"];
    
            // Obtener el ID de un registro de la tabla "rol"
            $sentenciaRol = $conexion->prepare("SELECT idRol FROM rol LIMIT 1");
            $sentenciaRol->execute();
            $idRol = $sentenciaRol->fetchColumn();
    
            // Verificar si el número de documento existe en la tabla "persona"
            $sentenciaVerificar = $conexion->prepare("SELECT COUNT(*) FROM persona WHERE numeroDocumento = :numeroDocumento");
            $sentenciaVerificar->bindParam(":numeroDocumento", $numeroDocumento);
            $sentenciaVerificar->execute();
            $existePersona = $sentenciaVerificar->fetchColumn();

            if ($existePersona) {
                // El número de documento existe en la tabla "persona", procede con la inserción en la tabla "usuario"
                // Insertar datos en la tabla "usuario"
                $sentenciaUsuario = $conexion->prepare("INSERT INTO usuario (contrasena, correo, celular, fechaDeRegistro, rol_idRol, persona_numeroDocumento) VALUES (:contrasena, :correo, :celular, :fechaDeRegistro, :rol_idRol, :persona_numeroDocumento)");
                $sentenciaUsuario->bindParam(":contrasena", $_POST["contrasena"]);
                $sentenciaUsuario->bindParam(":correo", $_POST["correo"]);
                $sentenciaUsuario->bindParam(":celular", $_POST["celular"]);
        
                $fechaDeRegistro = date("Y-m-d"); // Obtiene la fecha actual
                $sentenciaUsuario->bindParam(":fechaDeRegistro", $fechaDeRegistro);
        
                $sentenciaUsuario->bindParam(":rol_idRol", $idRol);
                $sentenciaUsuario->bindParam(":persona_numeroDocumento", $numeroDocumento);
                $sentenciaUsuario->execute();
            } else {
                echo "El número de documento ingresado no existe en la base de datos.";
            }
        } catch (PDOException $e) {
            // Verificar si se produjo una violación de clave primaria
            if ($e->getCode() == '23000' && $e->errorInfo[1] == 1062) {
                echo "El usuario ya existe. Por favor, elija otro correo o número de documento.";
            } else {
                echo "Error en la ejecución de la consulta: " . $e->getMessage();
            }
        }
        // Redireccionar al usuario después de la inserción exitosa
        header("Location:index.php");
    }

?>

<?php include("../../templates/header.php"); ?>

<br/>

<div class="card">
    <div class="card-header">
        Datos del usuario
    </div>
    <div class="card-body">

        <form action="" method="post" enctype="multipart/form.data">
            <div class="mb-3">
                <label for="tipoDocumento" class="form-label"> Tipo de documento: </label>
                <select name="tipoDocumento">
                    <option value="1">cedula de ciudadania</option>
                    <option value="2">cedula de extranjeria</option>
                </select>

            <script>
                var dropdown = document.getElementById("tipoDocumento");
                dropdown.addEventListener("change", function() {
                var selectTipoDocumento = dropdown.options[dropdown.selectedIndex].text;
            });
            </script>
            </div>

            <div class="mb-3">
                <label for="numeroDocumento" class="form-label">Numero de cedula</label>
                <input type="text"
                class="form-control" name="numeroDocumento" id="numeroDocumento" aria-describedby="helpId" placeholder="Número cedula">
            </div>
            <div class="mb-3">
                <label for="primerNombre" class="form-label">Primer nombre</label>
                <input type="text"
                class="form-control" name="primerNombre" id="primerNombre" aria-describedby="helpId" placeholder="Primer nombre">
            </div>

            <div class="mb-3">
                <label for="segundoNombre" class="form-label">Segundo nombre</label>
                <input type="text"
                class="form-control" name="segundoNombre" id="segundoNombre" aria-describedby="helpId" placeholder="Segundo nombre">
            </div>
            <div class="mb-3">
                <label for="primerApellido" class="form-label">Primer apellido</label>
                <input type="text"
                class="form-control" name="primerApellido" id="primerApellido" aria-describedby="helpId" placeholder="Primer apellido">
            </div>
            <div class="mb-3">
                <label for="segundoApellido" class="form-label">Segundo apellido</label>
                <input type="text"
                class="form-control" name="segundoApellido" id="segundoApellido" aria-describedby="helpId" placeholder="Segundo apellido">
            </div>



            <div class="mb-3">
                <label for="correo" class="form-label">Correo</label>
                <script>
                    function validarCorreo() {
                        var correo = document.getElementById("correo").value;
                        var mensaje = document.getElementById("mensaje");

                        if (correo.includes("@")) {
                            mensaje.innerHTML = "Correo válido";
                            mensaje.style.color = "green";
                        } else {
                            mensaje.innerHTML = "Correo inválido";
                            mensaje.style.color = "red";
                        }
                    }
                </script>
                <input type="email"
                class="form-control"  name="correo" id="correo" aria-describedby="helpId" placeholder="Escriba su correo" oninput="validarCorreo()">
            </div>

            <div class="mb-3">
                <label for="contrasena" class="form-label">Contraseña</label>
                <input type="password"
                class="form-control" name="contrasena" id="contrasena" aria-describedby="helpId" placeholder="Escriba una contraseña">
            </div>
            <div class="mb-3">
                <label for="celular" class="form-label">Celular</label>
                <input type="text"
                class="form-control" name="celular" id="celular" aria-describedby="helpId" placeholder="Celular">
            </div>
            <div class="mb-3">
                <label for="estrato" class="form-label">Estrato socioeconomico: </label>
            <select name="estrato">
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
            </select>

            <script>
                var dropdown = document.getElementById("estrato");

                dropdown.addEventListener("change", function() {
                var selectEstrato = dropdown.options[dropdown.selectedIndex].text;
                });
            </script>
            </div>
            <br>
            <div class="mb-3">
                <label for="paisNacimiento" class="form-label">Pais de nacimiento: </label>
            <select name="paisNacimiento">
            <option value="1">colombia</option>
            <option value="2">panama</option>
            </select>

            <script>
                var dropdown = document.getElementById("paisNacimiento");

                dropdown.addEventListener("change", function() {
                var selectPaisNacimiento = dropdown.options[dropdown.selectedIndex].text;
                });
            </script>
            </div>
            <br>
            <div class="mb-3">
                <label for="genero" class="form-label">Genero: </label>
            <select name="genero">
            <option value="1">M</option>
            <option value="2">F</option>
            <option value="3">Otro</option>
            </select>

            <script>
                var dropdown = document.getElementById("genero");

                dropdown.addEventListener("change", function() {
                var selectGenero = dropdown.options[dropdown.selectedIndex].text;
                });
            </script>
            </div>
            <br>
            <div class="mb-3">
                <label for="rh" class="form-label">Tipo de sangre: </label>
            <select name="rh">
            <option value="1">A+</option>
            <option value="2">O+</option>
            <option value="3">B+</option>
            <option value="4">AB+</option>
            <option value="5">A-</option>
            <option value="6">O-</option>
            <option value="7">B-</option>
            <option value="8">AB-</option>
            </select>

            <script>
                var dropdown = document.getElementById("rh");

                dropdown.addEventListener("change", function() {
                var selectRh = dropdown.options[dropdown.selectedIndex].text;
                });
            </script>
            </div>
            <br>
            <div class="mb-3">
                <label for="estadoCivil" class="form-label">Estado civil: </label>
            <select name="estadoCivil">
            <option value="1">soltero(a)</option>
            <option value="2">casado(a)</option>
            <option value="3">separado(a) o divorciado(a)</option>
            <option value="4">viudo</option>
            <option value="5">union libre</option>
            </select>

            <script>
                var dropdown = document.getElementById("estadoCivil");

                dropdown.addEventListener("change", function() {
                var selectREstadoCivil = dropdown.options[dropdown.selectedIndex].text;
                });
            </script>
            </div>
            <br>
            <div class="mb-3">
                <label for="telefono" class="form-label">Teléfono: </label>
                <input type="number"
                class="form-control" name="telefono" id="telefono" aria-describedby="helpId" placeholder="Teléfono">
            </div>
            <br>
            <div class="mb-3">
                <label for="direccion" class="form-label">Dirección: </label>
                <input type="text"
                class="form-control" name="direccion" id="direccion" aria-describedby="helpId" placeholder="Dirección">
            </div>
            <br>
            <label for="fechaNacimiento" class="form-label">Fecha de nacimiento: </label>
            <input type="date" name="fechaNacimiento" max="2005-12-31">

                <script>
                    var fechaNacimientoInput = document.getElementById("fechaNacimiento");

                    fechaNacimientoInput.addEventListener("change", function() {
                    var fechaNacimiento = fechaNacimientoInput.value;

                });
                </script>
                <br/>
                <br/>



            <button type="submit" class="btn btn-success">Agregar</button>
            <a name="" id="" class="btn btn-danger" href="index.php" role="button">Cancelar</a>

        </form>

    </div>
    <div class="card-footer text-muted">

    </div>
</div>

<?php include("../../templates/footer.php"); ?>