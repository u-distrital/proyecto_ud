<?php

include("../../bd.php");

if(isset($_GET['txtID'])){
    $txtID=(isset($_GET['txtID']))?$_GET['txtID']:"";

    $sentencia=$conexion->prepare(
        "SELECT * FROM usuarios WHERE idUsuario=:idUsuario"
    );

    $sentencia->bindParam(":idUsuario",$txtID);
    $sentencia->execute();
    $registro=$sentencia->fetch(PDO::FETCH_LAZY);
    
    $usuario=$registro["usuario"];
    $correo=$registro["correo"];
    $password=$registro["password"];

}

if($_POST){

    //Recolecta los datos del método POST
    $txtID=(isset($_POST["txtID"])?$_POST["txtID"]:"");
    $usuario=(isset($_POST["usuario"])?$_POST["usuario"]:"");
    $correo=(isset($_POST["correo"])?$_POST["correo"]:"");
    $password=(isset($_POST["password"])?$_POST["password"]:"");

    //Insercion de los datos
    $sentencia=$conexion->prepare(
        "UPDATE usuarios SET usuario=:usuario, correo=:correo, password=:password
            WHERE idUsuario=idUsuario"
    );

    //Asigna valores que tiene la variable :variable
    $sentencia->bindParam(":idUsuario",$txtID);
    $sentencia->bindParam(":usuario",$usuario);
    $sentencia->bindParam(":correo",$correo);
    $sentencia->bindParam(":password",$password);
    $sentencia->execute();
    header("Location:index.php");
}

?>


<?php include("../../templates/header.php"); ?>

<br/>

<div class="card">
    <div class="card-header">
        Datos del usuario
    </div>
    <div class="card-body">
        
        <form action="" method="post" enctype="multipart/form.data">

            <div class="mb-3">
                <label for="txtID" class="form-label">ID Usuario:</label>
                <input type="text"
                value="<?php echo $txtID; ?>"
                class="form-control" readonly name="txtID" id="txtID" aria-describedby="helpId" placeholder="ID del usuario">
            </div>

            <div class="mb-3">
                <label for="usuario" class="form-label">Nombre del usuario</label>
                <input type="text"
                value="<?php echo $usuario; ?>"
                class="form-control" name="usuario" id="usuario" aria-describedby="helpId" placeholder="Nombre del usuario">
            </div>

            <div class="mb-3">
                <label for="correo" class="form-label">Correo</label>
                <input type="email"
                value="<?php echo $correo; ?>"
                class="form-control" name="correo" id="correo" aria-describedby="helpId" placeholder="Escriba su correo">
            </div>

            <div class="mb-3">
                <label for="password" class="form-label">Password</label>
                <input type="password"
                value="<?php echo $password; ?>"
                class="form-control" name="password" id="password" aria-describedby="helpId" placeholder="Escriba una contraseña">
            </div>

            <button type="submit" class="btn btn-success">Agregar</button>
            <a name="" id="" class="btn btn-danger" href="index.php" role="button">Cancelar</a>

        </form>

    </div>
    <div class="card-footer text-muted">
        
    </div>
</div>

<?php include("../../templates/footer.php"); ?>