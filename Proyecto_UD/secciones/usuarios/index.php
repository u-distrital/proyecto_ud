<?php
//print_r($_POST);
    include("../../bd.php");

    //Borrar datos
    if(isset($_GET['txtID'])){
        $txtID=(isset($_GET['txtID']))?$_GET['txtID']:"";

        $sentencia=$conexion->prepare(
            "DELETE FROM usuario WHERE idUsuario=:idUsuario"
        );

        $sentencia->bindParam(":idUsuario",$txtID);
        $sentencia->execute();
        header("Location:index.php");

    }

    //Buscar datos
    $sentencia = $conexion->prepare("SELECT * FROM usuario");
    $sentencia->execute();
    $lista_usuarios=$sentencia->fetchAll(PDO::FETCH_ASSOC);

?>

<?php include("../../templates/header.php"); ?>

<br/>

<h1>Usuarios</h1>
<div class="card">
    <div class="card-header">
    <a name="" id="" class="btn btn-primary" href="crear.php" role="button">Agregar Usuario</a>
    </div>
    <div class="card-body">
        <div class="table-responsive-sm">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">ID usuario</th>
                        <th scope="col">Documento</th>
                        <th scope="col">Celular</th>
                        <th scope="col">Correo</th>
                        <th scope="col">Contraseña</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody>

                <?php foreach ($lista_usuarios as $registro) { ?>
                    <tr class="">
                        <td scope="row"><?php echo $registro['idUsuario'] ?></td>
                        <td><?php echo $registro['persona_numeroDocumento'] ?></td>
                        <td><?php echo $registro['celular'] ?></td>
                        <td><?php echo $registro['correo'] ?></td>
                        <td><?php echo $registro['contrasena'] ?></td>
                        <td>
                            <a class="btn btn-warning" href="editar.php?txtID=<?php echo $registro['idUsuario']; ?>" role="button">Editar</a>
                            <a class="btn btn-danger" href="index.php?txtID=<?php echo $registro['idUsuario']; ?>" role="button">Eliminar</a>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="card-footer text-muted">
        
    </div>
</div>

<?php include("../../templates/footer.php"); ?>