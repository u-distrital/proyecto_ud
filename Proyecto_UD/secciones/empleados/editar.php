<?php

include("../../bd.php");

if ($_POST) {
    // Recolecta los datos del método POST
    $numeroDocumento = $_POST["numeroDocumento"];
    $primerNombre = isset($_POST["primerNombre"]) ? $_POST["primerNombre"] : "";
    $primerApellido = isset($_POST["primerApellido"]) ? $_POST["primerApellido"] : "";
    $fechaDeNacimiento = isset($_POST["fechaDeNacimiento"]) ? $_POST["fechaDeNacimiento"] : "";
    $direccion = isset($_POST["direccion"]) ? $_POST["direccion"] : "";
    $telefono = isset($_POST["telefono"]) ? $_POST["telefono"] : "";

    // Actualiza los datos en la base de datos
    $sentencia = $conexion->prepare(
        "UPDATE persona SET primerNombre = :primerNombre, primerApellido = :primerApellido, fechaDeNacimiento = :fechaDeNacimiento, direccion = :direccion, telefono = :telefono WHERE numeroDocumento = :numeroDocumento"
    );

    $sentencia->bindParam(":numeroDocumento", $numeroDocumento);
    $sentencia->bindParam(":primerNombre", $primerNombre);
    $sentencia->bindParam(":primerApellido", $primerApellido);
    $sentencia->bindParam(":fechaDeNacimiento", $fechaDeNacimiento);
    $sentencia->bindParam(":direccion", $direccion);
    $sentencia->bindParam(":telefono", $telefono);

    $sentencia->execute();
    header("Location:index.php");
}

?>

<?php include("../../templates/header.php"); ?>
<br/>
<div class="card">
    <div class="card-header">
        Datos del empleado
    </div>
    <div class="card-body">
        <form action="" method="post" enctype_="multipart/form-data">

        <div class="mb-3">
            <label for="primerNombre" class="form-label">Primer Nombre</label>
            <input type="text"
            class="form-control" name="primerNombre" id="primerNombre" aria-describedby="helpId" placeholder="Primer Nombre">
        </div>

        <div class="mb-3">
            <label for="primerApellido" class="form-label">Primer Apellido</label>
            <input type="text"
            class="form-control" name="primerApellido" id="primerApellido" aria-describedby="helpId" placeholder="Primer Apellido">
        </div>

        <div class="mb-3">
            <label for="fechaDeNacimiento" class="form-label">Fecha Nacimiento</label>
            <input type="date" class="form-control" name="fechaDeNacimiento" id="fechaDeNacimiento" aria-describedby="emailHelpId" placeholder="Fecha de nacimiento">
        </div>

        <div class="mb-3">
            <label for="direccion" class="form-label">Dirección</label>
            <input type="text"
            class="form-control" name="direccion" id="direccion" aria-describedby="helpId" placeholder="Ej: TV 5 Sur">
        </div>

        <div class="mb-3">
            <label for="telefono" class="form-label">Teléfono</label>
            <input type="text"
            class="form-control" name="telefono" id="telefono" aria-describedby="helpId" placeholder="Número de telefono">
        </div>

        <!--<div class="card">
            <div class="card-header">
                Estudios
            </div>
            <div class="card-body">
                
                <div class="mb-3">
                    <label for="nombreinstitucion" class="form-label">Centro Educativo</label>
                    <input type="text"
                    class="form-control" name="nombreinstitucion" id="nombreinstitucion" aria-describedby="helpId" placeholder="Nombre Institución">
                </div>

                <div class="mb-3">
                    <label for="nivelestudios" class="form-label">Nivel de Estudios</label>
                    <select class="form-select form-select-sm" name="nivelestudios" id="nivelestudios">
                        <option selected>Seleccione</option>
                        <option value="">Educación Básica Primaria</option>
                        <option value="">Educación Básica Secundaria</option>
                        <option value="">Bachillerato / Educación Media</option>
                        <option value="">Universidad / Carrera técnica</option>
                        <option value="">Universidad / Carrera tecnológica</option>
                        <option value="">Universidad / Carrera Profesional</option>
                        <option value="">Postgrado / Especialización</option>
                        <option value="">Postgrado / Maestría</option>
                        <option value="">Postgrado / Doctorado</option>
                    </select>
                </div>

                <div class="mb-3">
                    <label for="cursadoactual" class="form-label">Cursando Actualmente</label>
                </div>

                <div class="mb-3">
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" id="cursadoactual" value="option1">
                        <label class="form-check-label" for="">Si</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" id="cursadoactual" value="option2">
                        <label class="form-check-label" for="">No</label>
                    </div>
                </div>

                <div class="mb-3">
                    <label for="fechainestudio" class="form-label">Desde</label>
                    <input type="date" class="form-control" name="fechainestudio" id="fechainestudio" aria-describedby="emailHelpId" placeholder="Fecha Inicio">
                </div>

                <div class="mb-3">
                    <label for="fechafiestudio" class="form-label">Hasta</label>
                    <input type="date" class="form-control" name="fechafiestudio" id="fechafiestudio" aria-describedby="emailHelpId" placeholder="Fecha Fin">
                </div>

            </div>
            <div class="card-footer text-muted">
                
            </div>
        </div>

        <br/>

        <div class="card">
            <div class="card-header">
                Experiencia Laboral
            </div>
            <div class="card-body">
                
                <div class="mb-3">
                    <label for="nombrecargo" class="form-label">Cargo</label>
                    <input type="text"
                    class="form-control" name="nombrecargo" id="nombrecargo" aria-describedby="helpId" placeholder="Ej: Auxiliar">
                </div>

                <div class="mb-3">
                    <label for="nombrempresa" class="form-label">Nombre de la empresa</label>
                    <input type="text"
                    class="form-control" name="nombrempresa" id="nombrempresa" aria-describedby="helpId" placeholder="Ej: Microsoft">
                </div>

                <div class="mb">
                    <label for="funcionescargo" class="form-label">Funciones y logros del cargo</label>
                </div>

                <div class="mb-3">
                    <label for="funcionescargo" class="form-label"></label>
                    <textarea class="form-control" name="funcionescargo" id="funcionescargo" rows="6" placeholder="Ej: Amplia experiencia en la área financiera de crédito. Análisis y aplicación de políticas financieras para la toma de decisiones, etc."></textarea>
                </div>

                <div class="mb-3">
                    <label for="cursadoactual" class="form-label">Sigue trabajando aqui</label>
                </div>

                <div class="mb-3">
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" id="trabajoactual" value="option1">
                        <label class="form-check-label" for="">Si</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" id="trabajoactual" value="option2">
                        <label class="form-check-label" for="">No</label>
                    </div>
                </div>

                <div class="mb-3">
                    <label for="fechaintrabajo" class="form-label">Desde</label>
                    <input type="date" class="form-control" name="fechaintrabajo" id="fechaintrabajo" aria-describedby="emailHelpId" placeholder="Fecha Inicio">
                </div>

                <div class="mb-3">
                    <label for="fechafitrabajo" class="form-label">Hasta</label>
                    <input type="date" class="form-control" name="fechafitrabajo" id="fechafitrabajo" aria-describedby="emailHelpId" placeholder="Fecha Fin">
                </div>

            </div>
            <div class="card-footer text-muted">
                
            </div>
        </div>-->

        <br/>

        <button type="submit" class="btn btn-success">Editar Registro</button>
        <a name="" id="" class="btn btn-danger" href="index.php" role="button">Cancelar</a>

        </form>
    </div>
    <div class="card-footer text-muted">
        
    </div>
</div>

<?php include("../../templates/footer.php"); ?>