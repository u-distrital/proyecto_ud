<?php

include("../../bd.php");

if($_POST){
    
    //Recolecta los datos del método POST
    $primerNombre=(isset($_POST["primerNombre"])?$_POST["primerNombre"]:"");
    $segundoNombre=(isset($_POST["segundoNombre"])?$_POST["segundoNombre"]:"");
    $primerApellido=(isset($_POST["primerApellido"])?$_POST["primerApellido"]:"");
    $segundoApellido=(isset($_POST["segundoApellido"])?$_POST["segundoApellido"]:"");

    
    $foto=(isset($_FILES["foto"]['name'])?$_FILES["foto"]['name']:"");
    $cv=(isset($_FILES["cv"]['name'])?$_FILES["cv"]['name']:"");

    
    $idPuesto=(isset($_POST["idPuesto"])?$_POST["idPuesto"]:"");
    $fechaIngreso=(isset($_POST["fechaIngreso"])?$_POST["fechaIngreso"]:"");

    //Insercion de los datos
    $sentencia=$conexion->prepare(
        "INSERT INTO empleados(idEmpleado, primerNombre, segundoNombre, primerApellido, segundoApellido, foto, cv, idPuesto, fechaIngreso)
            VALUES (null, :primerNombre, :segundoNombre, :primerApellido, :segundoApellido, :foto, :cv, :idPuesto, :fechaIngreso)"
    );

    $sentencia->bindParam(":primerNombre",$primerNombre);
    $sentencia->bindParam(":segundoNombre",$segundoNombre);
    $sentencia->bindParam(":primerApellido",$primerApellido);
    $sentencia->bindParam(":segundoApellido",$segundoApellido);

    $fecha_=new DateTime();

    $nombreArchivo_foto=($foto!='')?$fecha_->getTimestamp()."_".$_FILES["foto"]['name']:"";
    $tmp_foto=$_FILES["foto"]['name'];

    if($tmp_foto!=''){
        move_uploaded_file($tmp_foto,"./".$nombreArchivo_foto);
    }

    $sentencia->bindParam(":foto",$nombreArchivo_foto);

    $nombreArchivo_cv=($cv!='')?$fecha_->getTimestamp()."_".$_FILES["cv"]['name']:"";
    $tmp_cv=$_FILES["cv"]['name'];

    if($tmp_cv!=''){
        move_uploaded_file($tmp_cv,"./".$nombreArchivo_cv);
    }

    $sentencia->bindParam(":cv",$nombreArchivo_cv);


    $sentencia->bindParam(":idPuesto",$idPuesto);
    $sentencia->bindParam(":fechaIngreso",$fechaIngreso);

    $sentencia->execute();
    header("Location:index.php");

}

    $sentencia = $conexion->prepare("SELECT * FROM puestos");
    $sentencia->execute();
    $lista_puestos=$sentencia->fetchAll(PDO::FETCH_ASSOC);

?>

<?php include("../../templates/header.php"); ?>
<br/>
<div class="card">
    <div class="card-header">
        Datos del empleado
    </div>
    <div class="card-body">
        <form action="" method="post" enctype_="multipart/form-data">

        <div class="mb-3">
            <label for="primernombre" class="form-label">Primer Nombre</label>
            <input type="text"
            class="form-control" name="primernombre" id="primernombre" aria-describedby="helpId" placeholder="Primer Nombre">
        </div>

        <div class="mb-3">
            <label for="segundonombre" class="form-label">Segundo Nombre</label>
            <input type="text"
            class="form-control" name="segundonombre" id="segundonombre" aria-describedby="helpId" placeholder="Segundo Nombre">
        </div>

        <div class="mb-3">
            <label for="primerapellido" class="form-label">Primer Apellido</label>
            <input type="text"
            class="form-control" name="primerapellido" id="primerapellido" aria-describedby="helpId" placeholder="Primer Apellido">
        </div>

        <div class="mb-3">
            <label for="segundoapellido" class="form-label">Segundo Apellido</label>
            <input type="text"
            class="form-control" name="segundoapellido" id="segundoapellido" aria-describedby="helpId" placeholder="Segundo Apellido">
        </div>

        <div class="mb-3">
            <label for="" class="form-label">Foto:</label>
            <input type="file"
            class="form-control" name="foto" id="foto" aria-describedby="helpId" placeholder="Foto">
        </div>

        <div class="mb-3">
            <label for="" class="form-label">CV(PDF):</label>
            <input type="file" class="form-control" name="cv" id="cv" placeholder="Hoja de Vida" aria-describedby="fileHelpId">
        </div>

        <div class="mb-3">
            <label for="idpuesto" class="form-label">Puesto:</label>
            <select class="form-select form-select-sm" name="idpuesto" id="idpuesto">
                <?php foreach($lista_puestos as $registro) { ?>
                    <option value="<?php echo $registro['idPuesto'];?>">
                        <?php echo $registro['nombrePuesto'];?>
                    </option>
                <?php } ?>
            </select>
        </div>

        <div class="mb-3">
            <label for="fechaingreso" class="form-label">Fecha Ingreso:</label>
            <input type="date" class="form-control" name="fechaingreso" id="fechaingreso" aria-describedby="emailHelpId" placeholder="Fecha Ingreso">
        </div>

        <div class="card">
            <div class="card-header">
                Estudios
            </div>
            <div class="card-body">
                
                <div class="mb-3">
                    <label for="nombreinstitucion" class="form-label">Centro Educativo</label>
                    <input type="text"
                    class="form-control" name="nombreinstitucion" id="nombreinstitucion" aria-describedby="helpId" placeholder="Nombre Institución">
                </div>

                <div class="mb-3">
                    <label for="nivelestudios" class="form-label">Nivel de Estudios</label>
                    <select class="form-select form-select-sm" name="nivelestudios" id="nivelestudios">
                        <option selected>Seleccione</option>
                        <option value="">Educación Básica Primaria</option>
                        <option value="">Educación Básica Secundaria</option>
                        <option value="">Bachillerato / Educación Media</option>
                        <option value="">Universidad / Carrera técnica</option>
                        <option value="">Universidad / Carrera tecnológica</option>
                        <option value="">Universidad / Carrera Profesional</option>
                        <option value="">Postgrado / Especialización</option>
                        <option value="">Postgrado / Maestría</option>
                        <option value="">Postgrado / Doctorado</option>
                    </select>
                </div>

                <div class="mb-3">
                    <label for="cursadoactual" class="form-label">Cursando Actualmente</label>
                </div>

                <div class="mb-3">
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" id="cursadoactual" value="option1">
                        <label class="form-check-label" for="">Si</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" id="cursadoactual" value="option2">
                        <label class="form-check-label" for="">No</label>
                    </div>
                </div>

                <div class="mb-3">
                    <label for="fechainestudio" class="form-label">Desde</label>
                    <input type="date" class="form-control" name="fechainestudio" id="fechainestudio" aria-describedby="emailHelpId" placeholder="Fecha Inicio">
                </div>

                <div class="mb-3">
                    <label for="fechafiestudio" class="form-label">Hasta</label>
                    <input type="date" class="form-control" name="fechafiestudio" id="fechafiestudio" aria-describedby="emailHelpId" placeholder="Fecha Fin">
                </div>

            </div>
            <div class="card-footer text-muted">
                
            </div>
        </div>

        <br/>

        <div class="card">
            <div class="card-header">
                Experiencia Laboral
            </div>
            <div class="card-body">
                
                <div class="mb-3">
                    <label for="nombrecargo" class="form-label">Cargo</label>
                    <input type="text"
                    class="form-control" name="nombrecargo" id="nombrecargo" aria-describedby="helpId" placeholder="Ej: Auxiliar">
                </div>

                <div class="mb-3">
                    <label for="nombrempresa" class="form-label">Nombre de la empresa</label>
                    <input type="text"
                    class="form-control" name="nombrempresa" id="nombrempresa" aria-describedby="helpId" placeholder="Ej: Microsoft">
                </div>

                <div class="mb">
                    <label for="funcionescargo" class="form-label">Funciones y logros del cargo</label>
                </div>

                <div class="mb-3">
                    <label for="funcionescargo" class="form-label"></label>
                    <textarea class="form-control" name="funcionescargo" id="funcionescargo" rows="6" placeholder="Ej: Amplia experiencia en la área financiera de crédito. Análisis y aplicación de políticas financieras para la toma de decisiones, etc."></textarea>
                </div>

                <div class="mb-3">
                    <label for="cursadoactual" class="form-label">Sigue trabajando aqui</label>
                </div>

                <div class="mb-3">
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" id="trabajoactual" value="option1">
                        <label class="form-check-label" for="">Si</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" id="trabajoactual" value="option2">
                        <label class="form-check-label" for="">No</label>
                    </div>
                </div>

                <div class="mb-3">
                    <label for="fechaintrabajo" class="form-label">Desde</label>
                    <input type="date" class="form-control" name="fechaintrabajo" id="fechaintrabajo" aria-describedby="emailHelpId" placeholder="Fecha Inicio">
                </div>

                <div class="mb-3">
                    <label for="fechafitrabajo" class="form-label">Hasta</label>
                    <input type="date" class="form-control" name="fechafitrabajo" id="fechafitrabajo" aria-describedby="emailHelpId" placeholder="Fecha Fin">
                </div>

            </div>
            <div class="card-footer text-muted">
                
            </div>
        </div>

        <br/>

        <button type="submit" class="btn btn-success">Agregar Registro</button>
        <a name="" id="" class="btn btn-danger" href="index.php" role="button">Cancelar</a>

        </form>
    </div>
    <div class="card-footer text-muted">
        
    </div>
</div>

<?php include("../../templates/footer.php"); ?>