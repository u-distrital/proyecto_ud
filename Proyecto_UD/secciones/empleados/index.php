<?php

include("../../bd.php");

//Borrar datos
if(isset($_GET['txtID'])){
    $txtID=(isset($_GET['txtID']))?$_GET['txtID']:"";

    //Busca el archivo del empleado
    $sentencia = $conexion->prepare("SELECT * FROM persona");
    $sentencia->execute();
    $lista_puestos=$sentencia->fetchAll(PDO::FETCH_ASSOC);

    /*
    $sentencia=$conexion->prepare(
        "DELETE FROM empleados WHERE idEmpleado=:idEmpleado"
    );

    $sentencia->bindParam(":idPuesto",$txtID);
    $sentencia->execute();
    header("Location:index.php");
    */
}

//Buscar datos
$sentencia = $conexion->prepare("SELECT * FROM persona");
$sentencia->execute();
$lista_empleados=$sentencia->fetchAll(PDO::FETCH_ASSOC);

?>


<?php include("../../templates/header.php"); ?>

<br/>
<h1>Empleados</h1>
<div class="card">
    <div class="card-header">
        <a name="" id="" class="btn btn-primary" href="crear.php" role="button">Agregar Registro</a>
    </div>
    <div class="card-body">
        <div class="table-responsive-sm">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Documento</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Apellido</th>
                        <th scope="col">Fecha de nacimiento</th>
                        <th scope="col">Dirección</th>
                        <th scope="col">Teléfono</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody>

                    <?php foreach($lista_empleados as $registro) { ?>

                        <tr class="">
                            <td><?php echo $registro['numeroDocumento'];?></td>
                            <td scope="row"><?php echo $registro['primerNombre'];?></td>
                            <td><?php echo $registro['primerApellido'];?></td>
                            <td><?php echo $registro['fechaDeNacimiento'];?></td>
                            <td><?php echo $registro['direccion'];?></td>
                            <td><?php echo $registro['telefono'];?></td>
                            <td>
                                <a class="btn btn-warning" href="editar.php?txtID=<?php echo $registro['numeroDocumento']; ?>" role="button">Editar</a>
                                <!--<a class="btn btn-danger" href="index.php?txtID=<?php echo $registro['numeroDocumento']; ?>" role="button">Eliminar</a>-->
                            </td>
                        </tr>

                    <?php } ?>
                </tbody>
            </table>
        </div>

    </div>
</div>

<?php include("../../templates/footer.php"); ?>