<?php 

include("../../bd.php");

    if(isset($_GET['txtID'])){
        $txtID=(isset($_GET['txtID']))?$_GET['txtID']:"";

        $sentencia=$conexion->prepare(
            "SELECT * FROM puestos WHERE idPuesto=:idPuesto"
        );

        $sentencia->bindParam(":idPuesto",$txtID);
        $sentencia->execute();
        $registro=$sentencia->fetch(PDO::FETCH_LAZY);
        $nombrePuesto=$registro["nombrePuesto"];

    }

    if($_POST){

        //Recolecta los datos del método POST
        $txtID=(isset($_POST['txtID']))?$_POST['txtID']:"";
        $nombrePuesto=(isset($_POST["nombrePuesto"])?$_POST["nombrePuesto"]:"");

        //Insercion de los datos
        $sentencia=$conexion->prepare(
            "UPDATE puestos SET nombrePuesto=:nombrePuesto
                WHERE idPuesto=:idPuesto"
        );

        //Asigna los valores del metodo POST (Que vienen del formulario)
        $sentencia->bindParam(":nombrePuesto",$nombrePuesto);
        $sentencia->bindParam(":idPuesto",$txtID);
        $sentencia->execute();
        header("Location:index.php");
    }

?>


<?php include("../../templates/header.php"); ?>

<br/>

<div class="card">
    <div class="card-header">
        Editar Puesto
    </div>
    <div class="card-body">
        
        <form action="" method="post" enctype="multipart/form.data">

            <div class="mb-3">
                <label for="txtID" class="form-label">ID Puesto:</label>
                <input type="text"
                value="<?php echo $txtID; ?>"
                class="form-control" readonly name="txtID" id="txtID" aria-describedby="helpId" placeholder="ID del puesto">
            </div>

            <div class="mb-3">
                <label for="nombrepuesto" class="form-label">Nombre del puesto</label>
                <input type="text"
                value="<?php echo $nombrePuesto; ?>"
                class="form-control" name="nombrepuesto" id="nombrepuesto" aria-describedby="helpId" placeholder="Nombre del puesto">
            </div>

            <button type="submit" class="btn btn-success">Actualizar</button>
            <a name="" id="" class="btn btn-danger" href="index.php" role="button">Cancelar</a>

        </form>

    </div>
    <div class="card-footer text-muted">
        
    </div>
</div>

<?php include("../../templates/footer.php"); ?>