<?php

    include("../../bd.php");

    if($_POST){
        print_r($_POST);

        //Recolecta los datos del método POST
        $nombrePuesto=(isset($_POST["nombrePuesto"])?$_POST["nombrePuesto"]:"");

        //Insercion de los datos
        $sentencia=$conexion->prepare(
            "INSERT INTO puestos(idPuesto, nombrePuesto)
                VALUES (null, :nombrePuesto)"
        );

        //Asigna los valores del metodo POST (Que vienen del formulario)
        $sentencia->bindParam(":nombrePuesto",$nombrePuesto);
        $sentencia->execute();
        header("Location:index.php");
    }

?>

<?php include("../../templates/header.php"); ?>

<br/>

<div class="card">
    <div class="card-header">
        Puestos
    </div>
    <div class="card-body">
        
        <form action="" method="post" enctype="multipart/form.data">

            <div class="mb-3">
                <label for="nombrepuesto" class="form-label">Nombre del puesto</label>
                <input type="text"
                class="form-control" name="nombrepuesto" id="nombrepuesto" aria-describedby="helpId" placeholder="Nombre del puesto">
            </div>

            <button type="submit" class="btn btn-success">Agregar</button>
            <a name="" id="" class="btn btn-danger" href="index.php" role="button">Cancelar</a>

        </form>

    </div>
    <div class="card-footer text-muted">
        
    </div>
</div>

<?php include("../../templates/footer.php"); ?>