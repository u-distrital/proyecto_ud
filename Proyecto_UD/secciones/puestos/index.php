<?php

    include("../../bd.php");


    //Borrar datos
    if(isset($_GET['txtID'])){
        $txtID=(isset($_GET['txtID']))?$_GET['txtID']:"";

        $sentencia=$conexion->prepare(
            "DELETE FROM puestos WHERE idPuesto=:idPuesto"
        );

        $sentencia->bindParam(":idPuesto",$txtID);
        $sentencia->execute();
        header("Location:index.php");

    }

    //Buscar datos
    $sentencia = $conexion->prepare("SELECT * FROM puestos");
    $sentencia->execute();
    $lista_puestos=$sentencia->fetchAll(PDO::FETCH_ASSOC);

?>
<?php include("../../templates/header.php"); ?>

<br/>

<h1>Puestos</h1>
<div class="card">
    <div class="card-header">
    <a name="" id="" class="btn btn-primary" href="crear.php" role="button">Agregar Registro</a>
    </div>
    <div class="card-body">
        <div class="table-responsive-sm">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Nombre del puesto</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody>

                    <?php foreach($lista_puestos as $registro) { ?>

                        <tr class="">
                            <td scope="row"><?php echo $registro['idPuesto'];?></td>
                            <td><?php echo $registro['nombrePuesto'];?></td>
                            <td>
                                <a class="btn btn-warning" href="editar.php?txtID=<?php echo $registro['idPuesto']; ?>" role="button">Editar</a>
                                <a class="btn btn-danger" href="index.php?txtID=<?php echo $registro['idPuesto']; ?>" role="button">Eliminar</a>
                            </td>
                        </tr>
                        <tr class="">
                            <td scope="row">1922</td>
                            <td>Analista NOC</td>
                            <td>
                                <input name="btneditar" id="btneditar" class="btn btn-warning" type="button" value="Editar">
                                <input name="btneliminar" id="btneliminar" class="btn btn-danger" type="button" value="Eliminar">
                            </td>
                        </tr>

                        <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="card-footer text-muted">
        
    </div>
</div>


<?php include("../../templates/footer.php"); ?>