<?php

session_start();

if($_POST)
{
  include("./bd.php");
    $sentencia = $conexion->prepare("SELECT *, count(*) AS n_usuario, persona_numeroDocumento
    FROM usuario
    WHERE persona_numeroDocumento = :persona_numeroDocumento
    AND contrasena = :contrasena");


    $usuario=$_POST["persona_numeroDocumento"];
    $password=$_POST["contrasena"];

    $sentencia->bindParam(":persona_numeroDocumento",$usuario);
    $sentencia->bindParam(":contrasena",$password);


    $sentencia->execute();

    $registro=$sentencia->fetch(PDO::FETCH_LAZY);
    if($registro->n_usuario > 0){
            $_SESSION['persona_numeroDocumento']=$registro["persona_numeroDocumento"];
            $_SESSION['logueado']=true;

            header("Location:index.php");
            exit(); // Importante: asegúrate de salir del script después de redireccionar
    }else{
      $mensaje="Error, el usuario o contraseña son incorrectos, por favor contacte al administrador";
    }


}
?>


<!DOCTYPE html>
<html>
<head>
  <title>Login PayWise</title>

  <link rel="stylesheet" type="text/css" href="style/estiloLogin.css">
</head>
  <body>

  

    <div class="wrapper fadeInDown">
    <div class="sliding-image">
      <img src="img/logo letra.jpg" alt="Imagen deslizante" width="450px">
    </div>
      <div id="formContent">
        
        <!-- Tabs Titles -->
        <h2 class="active"> Iniciar Sesión </h2>

        <?php if(isset($mensaje)){ ?>
            <div class="alert alert-danger" role="alert">
              <strong><?php echo $mensaje;?></strong>
        </div>
        <?php } ?>

        <br>
        <br>
        <br>
        <!-- Login Form -->
        <form method="POST" action="">
          <input type="text" name ="persona_numeroDocumento" id="persona_numeroDocumento" class="fadeIn second" placeholder="Usuario" required>
          <br>
          <br>
          <input type="password" name="contrasena" id="contrasena" class="fadeIn third" placeholder="Contraseña" required>
          <br>
          <br>
          <input type="submit" class="fadeIn fourth" value="ENTRAR">
        </form>
      </div>
    </div>
  </body>
</html>
